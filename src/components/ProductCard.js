import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useContext, useState } from "react";

import UserContext from "../UserContext";
import { base_url } from "../utils/axios";

export default function ProductCard({ product }) {
  const { user } = useContext(UserContext);

  const isAdmin = user.email === "email";

  const { isActive } = product ?? { isAdmin: false, isActive: true };
  const { _id, name, description, price } = product;
  const [isProductActive, setIsProductActive] = useState(isActive);

  console.log(product);

  const handleClick = () => {
    fetch(`${base_url}/products/${_id}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: !isProductActive,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setIsProductActive(!isProductActive);
      })
      .catch((error) => {
        console.log("Error product:", error);
      });
  };

  return (
    <Card className="my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>

        {user.isAdmin ? (
          <>
            <Link className="btn btn-primary" to={`/products/listing/${_id}`}>
              Details
            </Link>{" "}
            {""}
            <Button
              variant={isProductActive ? "success" : "danger"}
              onClick={handleClick}
            >
              {isProductActive ? "Active" : "Inactive"}
            </Button>
          </>
        ) : (
          <>
            <Link className="btn btn-primary" to={`/products/${_id}`}>
              Details
            </Link>
          </>
        )}
      </Card.Body>
    </Card>
  );
}
