import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { base_url } from "../utils/axios";

export default function ProductListing({ product }) {
  const { _id: productId } = useParams();

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const purchase = (productId) => {
    fetch(`${base_url}/users/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Successfull Purchase",
            icon: "success",
            text: "You have successfully pruchased this product.",
          });

          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  useEffect(() => {
    fetch(`${base_url}/products/${productId}/`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  console.log(productId);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 5, offset: 3 }}>
          <Card className="my-3">
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <Card.Subtitle>Store Hours</Card.Subtitle>
              <Card.Text>8 am - 5 pm</Card.Text>
              <Link
                className="btn btn-primary"
                to={`/products/admin/update/${productId}`}
              >
                Update
              </Link>{" "}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
