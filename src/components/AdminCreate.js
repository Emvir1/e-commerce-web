import { useState } from "react";
import { Form, Button, Alert } from "react-bootstrap";
import { base_url } from "../utils/axios";

export default function AdminCreate() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch(`${base_url}/products/createproduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => {
        if (res.ok) {
          setSuccess(true);
          setName("");
          setDescription("");
          setPrice(0);
        } else {
          throw new Error("Failed to create product");
        }
      })
      .catch((err) => {
        setError(err.message);
      });
  };

  return (
    <>
      <h1>Create Product</h1>
      {success && (
        <Alert variant="success" onClose={() => setSuccess(false)} dismissible>
          Product created successfully!
        </Alert>
      )}
      {error && (
        <Alert variant="danger" onClose={() => setError("")} dismissible>
          {error}
        </Alert>
      )}
      <Form onSubmit={(e) => handleSubmit(e)}>
        <Form.Group controlId="firstName">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Product Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="lastName">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit" id="submitBtn">
          Create
        </Button>
      </Form>
    </>
  );
}
