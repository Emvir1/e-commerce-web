import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { useParams, useNavigate, Link } from "react-router-dom";
import { Form, Button, Alert } from "react-bootstrap";
import Swal from "sweetalert2";
import { base_url } from "../utils/axios";

export default function AdminUpdate() {
  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    console.log(productId);

    e.preventDefault();

    fetch(`${base_url}/products/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Successfully updated",
            icon: "success",
            text: "You have successfully updated the product.",
          });
          navigate("/products/admin");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  return (
    <>
      <h1>Update Product</h1>
      <Form onSubmit={(e) => handleSubmit(e)}>
        <Form.Group controlId="Name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Update Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="lastName">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Update Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="tel"
            placeholder="Update price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Button variant="primary" type="submit" id="submitBtn">
          Update
        </Button>
      </Form>
    </>
  );
}
