import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import productImg1 from "../images/mouse.jpg";
import productImg2 from "../images/monitor.png";
import productImg3 from "../images/keyboard.jpg";
import productImg4 from "../images/mousepad.webp";
import productImg5 from "../images/avr.jpg";
import productImg6 from "../images/mouse2.webp";
import { base_url } from "../utils/axios";

export default function ProductView() {
  const { productId } = useParams();

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [product, setProduct] = useState(null);
  const [productName, setProductName] = useState("");
  const [quantity, setQuantity] = useState(1);
  const [count, setCount] = useState(0);

  const productImages = [
    productImg1,
    productImg2,
    productImg3,
    productImg4,
    productImg5,
    productImg6,
  ];
  const randomIndex = Math.floor(Math.random() * productImages.length);
  const randomProductImage = productImages[randomIndex];

  const purchase = (productId) => {
    fetch(`${base_url}/users/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify([
        {
          productId: productId,
          productName: productName,
          quantity: quantity,
          price: product.price,
          userId: user.id,
        },
      ]),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Successfull Purchase",
            icon: "success",
            text: "You have successfully pruchased this product.",
          });

          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  const buy = () => {
    if (quantity > 0) {
      setCount(count + 1);
      setQuantity(quantity - 1);
    } else {
      Swal.fire({
        title: "Out of Stock",
        icon: "error",
        text: "Purchase other products",
      });
    }
  };

  useEffect(() => {
    fetch(`${base_url}/products/${productId}/`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
        setProductName(data.name);
      });
  }, [productId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 5, offset: 3 }}>
          {product && (
            <Card className="my-3">
              <Card.Img src={randomProductImage} />
              <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{product.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {product.price}</Card.Text>
                <Card.Subtitle>Store Hours</Card.Subtitle>
                <Card.Text>8 am - 5 pm</Card.Text>
                <Card.Subtitle>Quantity:</Card.Subtitle>
                <input
                  type="number"
                  min="1"
                  max={product.quantity}
                  value={quantity}
                  onChange={(e) => setQuantity(parseInt(e.target.value))}
                  className="form-control mb-3"
                  style={{ width: "100px" }}
                />
                <Card.Text>{product.quantity}</Card.Text>
                {user.id !== null ? (
                  <Button
                    variant="primary"
                    block
                    onClick={() => purchase(productId)}
                  >
                    Buy now!
                  </Button>
                ) : (
                  <Link className="btn btn-danger btn-block" to="/login">
                    Log in to Buy now!
                  </Link>
                )}
              </Card.Body>
            </Card>
          )}
        </Col>
      </Row>
    </Container>
  );
}
