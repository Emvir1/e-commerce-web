
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from 'react';

import UserContext from '../UserContext';

export default function UpdateProductCard({ product }) {

    const { user } = useContext(UserContext);

    const isAdmin = user.email === 'email';

    const { _id, name, description, price } = product;

    console.log(product);


    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{ price }</Card.Text>

                <Link className="btn btn-primary" to={`/product/${_id}`}>Update</Link>
            </Card.Body>
        </Card>
    );
}
