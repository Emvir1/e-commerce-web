import { useState, useEffect } from "react";

import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

import "./App.css";

import AppNavbar from "./components/AppNavbar";
import Products from "./pages/Products";
import ProductView from "./components/ProductView";
import ProductListing from "./components/ProductListing";
import AdminArchive from "./components/AdminArchive";
import AdminCreate from "./components/AdminCreate";
import AdminUpdate from "./components/AdminUpdate";
import UpdateProductCard from "./components/UpdateProductCard";
import Footer from "./components/Footer";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import AdminDashboard from "./pages/AdminDashboard";
import AdminProduct from "./pages/AdminProduct";
import { base_url } from "./utils/axios";

import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${base_url}/users/details`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container fluid>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/products/listing/:_id" element={<ProductListing />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/register" element={<Register />} />
            <Route path="/products/admin" element={<AdminDashboard />} />
            <Route path="/products/admin/archive" element={<AdminArchive />} />
            <Route path="/products/admin/create" element={<AdminCreate />} />
            <Route
              path="/products/admin/update/:productId"
              element={<AdminUpdate />}
            />
            <Route path="/products/all" element={<AdminProduct />} />
            <Route path="/product/:productId" element={<UpdateProductCard />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
