import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Page Not Found",
        content: "Go back to the ",
        destination: "/",
        label: "homepage"
    }
    
    return (
        <Banner data={data}/>
    )
}