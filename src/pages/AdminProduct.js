import { useEffect, useState } from "react";
import AdminUpdate from "../components/AdminUpdate";
// import UpdateProductCard from '../components/AdminUpdate';
import { base_url } from "../utils/axios";

export default function AdminProduct() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${base_url}/products/hardware`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <AdminUpdate key={product._id} product={product} />;
          })
        );
      });
  }, []);
  return <>{products}</>;
}
