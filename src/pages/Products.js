import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { base_url } from "../utils/axios";

export default function Products() {
  const [product, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${base_url}/products`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} product={product} />;
          })
        );
      });
  }, []);

  return <>{product}</>;
}
